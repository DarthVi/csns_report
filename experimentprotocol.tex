\section{Experiments}
\label{sec:experiments}
Experiments have been made mainly on three kinds of network layouts: 
\begin{itemize}
	\item Spatially clustered network layouts, which resemble the layout of real societies and cities; nodes within the same block or neighborhood have an high probability of being connected, while neighborhoods themselves are usually scarcely connected among them, with few nodes that act as bridges. This kind of layout also allow us to investigate the potential correlation between Louvain communities\cite{louvain:community} and the way cultural dissemination and segregation work.
	\item 2D lattice, whose layout resembles the grid based one that is characteristic of cities in USA. Moreover this kind of network layout allows us to compare the results with the original studies of Axelrod\cite{axelrod:model} and Schelling\cite{schelling:model}.
	\item Preferential attachment\cite{barabasi}\cite{barabasi:albert}, whose rich-get-richer scheme is of interest in order to study what happens for cultures in real life social networks, where a few influencers have the majority of connections with other people.
\end{itemize}

For each previously mentioned layouts the experiments have been conducted following the protocol and procedures discussed in the following subsection. In figures empty sites are always represented in white.

%TODO if you make more experiments on other layouts (like preferential attachment), add here the info

\subsection{Protocol and procedures for the experiments}
\label{subsec:protocol}
Each experiments, carried out with a specific configuration of the parameters, has been repeated at least 3 times with different random seeds (which will be shown in the figures of this paper in order to ensure replicability) to ensure that the outcomes are not due to statistical anomalies. Here the choices for the values will be explained; testing configurations are obtained by considering every combination of these parameter values.


Choices for $q$ value and $F$ value are done with the idea of testing the prevision \ref{itm:prev6} and \ref{itm:prev9} listed in the previsions section \ref{subsec:previsions}:
\begin{itemize}
	\item $q = 3$ and $F = 3$: this cultural code configuration has been chosen to see what happens with low cultural code complexity.
	\item $q = 9$ and $F = 3$: the code length is still simple, but we want to see what happens with more complex cultural traits.
	\item $q = 3$ and $F = 9$: this is the opposite of the previous choice, we have few values for the traits but a longer cultural code.
	\item $q = 9$ and $F = 9$
	\item $q = 15$ and $F = 15$: in both this case and the previous one, both code length and possible traits values have been increased. No experiments have been done on values higher than 15, because they rarely are interesting and, has mentioned in the prevision \ref{itm:prev6} and confirmed by the experiments, the time needed for the simulation may increase too much.
\end{itemize}

In order to observe the differences between hyper-connected societies and less connected ones (see prevision \ref{itm:prev1} and \ref{itm:prev2} of the prevision section \ref{subsec:previsions}), two different values of the average node degree, specifically $6$ and $12$, have been used for the spatially clustered network layout.


Empty probability influences the possibility for nodes to improve their average cultural overlap by moving to a new site without having to resort mainly on imitation of neighbours:
\begin{itemize}
	\item 0.10: very low empty probability; we want to see what happens when nodes must rely almost exclusively on imitations rather than moving.
	\item 0.30: low empty probability.
	\item 0.45: higher empty probability, very close to one half.
\end{itemize}

The $T$ threshold tells us how tolerant the society is. Values for this parameter have been chosen with the idea of testing the expectations \ref{itm:prev4}, \ref{itm:prev5} and \ref{itm:prev7} listed in section \ref{subsec:previsions}.
\begin{itemize}
	\item 0.10: extremely tolerant society, within the boundary of prevision \ref{itm:prev7}.
	\item 0.20: very tolerant society, within the boundary of prevision \ref{itm:prev7}.
	\item 0.30: still tolerant, but outside the boundary of prevision \ref{itm:prev7}.
	\item 0.51 and 0.55: as previously said, these are fairly reasonable values for an individual who may want to be tolerant but at the same time be satisfied and have at least almost half of his peers similar to his culture. These values are also used to take in consideration the expectation \ref{itm:prev4}.
	\item 0.80: it has been chosen to observe the behaviour of intolerant societies.
\end{itemize}

Very few experiment have been conducted with $T = 1$ because it produces obvious results. Moreover most of the experiments have been done with 400 nodes, with few of them with 150 nodes. The NetLogo simulation built for these experiments can theoretically handle 1000 nodes maximum, but due to the high connectivity of some layouts and the complexity of the code used to collect statistical data for the plots and to handle the color coding, the simulation may be slow when too many nodes are used. For this reason no tests have been carried out with more than 400 nodes.

\subsection{Results and findings}
\label{subsec:results}
Due to the high number of experiments (3, sometimes 5, for each configuration of the parameters) it is not possible to show all the experimental results. However it is possible to show specific experiments that are useful to confirm, deny or clarify the previsions made in section \ref{subsec:previsions}, allowing us to understand better the behaviour of the model.

\subsubsection{Spatially clustered network layout: }
as expected by prevision \ref{itm:prev1}, hyper-connectivity plays an important role to obtain cultural homogeneity. However hyper-connectivity alone usually isn't enough, and the $T$ threshold and the empty probability are equally important. Indeed counterintuitively the lower the $T$ threshold and the empty probability are the more likely imitations are. This is due to the fact that if imitation does not happen at time step $t$, with a low $T$ threshold few similar individual in the neighborhood are needed in order not to force the individual to move, increasing the chance for imitation to occur at time step $t + 1$. Even if the individual does move, the number of empty sites is not high enough to consider moving a good strategy to improve the average cultural overlap. For this reason, homogeneity may result also from well connected societies, not necessarily hyper-connected, provided that the tolerance is high enough and the number of empty sites is low. All of this is evident in Figure \ref{fig:img1}.
Instead Figure \ref{fig:img2} and Figure \ref{fig:img3} show that hyper-connectivity becomes a discriminant factor to predict what happens when the empty probability is high enough and the $T$ threshold is still low. More specifically, these experiment confirm previsions \ref{itm:prev1}, \ref{itm:prev2} and \ref{itm:prev7}, allowing us to formulate the following conclusion:

\begin{proposition}
	In highly clustered societies (clustering coefficient significantly higher than the edge density) it is possible to have heterogeneity (more cultures in the equilibrium state) with sufficient empty sites and low $T$ threshold (around $0.20$, tolerant society). However if the society is hyper-connected (sufficiently high average node degree), the network will reach an equilibrium state characterized by homogeneity, even with sufficient empty sites and a tolerant society.
\end{proposition}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/img1_3.png}
	\begin{minipage}[b]{.30\textwidth}
		\includegraphics[width=\linewidth]{images/img1_1.png}
	\end{minipage}
	\begin{minipage}[b]{.295\textwidth}
		\includegraphics[width=\linewidth]{images/img1_2.png}
	\end{minipage}%
	\caption{\textbf{Bottom left}: starting population of a spatially clustered network with 400 nodes with average degree 6, $e=0.10$, $T=0.10$, $F=3$, $q=3$; the seed for this network is $-2037327187$, there are 27 cultures in this initial state. \textbf{Bottom right}: final population at the equilibrium state, from 27 different cultural codes randomly spread over the population, the simulation reached an equilibrium with 2 culture, one of which prevails over the other. \textbf{Top}: data collected during the simulation.}
	\label{fig:img1}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/img2_3.png}
	\begin{minipage}[b]{.30\textwidth}
		\includegraphics[width=\linewidth]{images/img2_1.png}
	\end{minipage}
	\begin{minipage}[b]{.30\textwidth}
		\includegraphics[width=\linewidth]{images/img2_2.png}
	\end{minipage}%
	\caption{\textbf{Bottom left}: starting population of a spatially clustered network with 400 nodes with average degree 6, $e=0.35$, $T=0.20$, $F=15$, $q=15$; the seed for this network is $-193415129$, there are 280 cultures in this initial state. \textbf{Bottom right}: final population at the equilibrium state, from 280 different cultural codes randomly spread over the population, the simulation reached an equilibrium with 10 culture. \textbf{Top}: data collected during the simulation.}
	\label{fig:img2}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/img3_3.png}
	\begin{minipage}[b]{.30\textwidth}
		\includegraphics[width=\linewidth]{images/img3_1.png}
	\end{minipage}
	\begin{minipage}[b]{.30\textwidth}
		\includegraphics[width=\linewidth]{images/img3_2.png}
	\end{minipage}%
	\caption{\textbf{Bottom left}: starting population of a spatially clustered network with 400 nodes with average degree 12, $e=0.35$, $T=0.20$, $F=15$, $q=15$; the seed for this network is $-1208982709$, there are 259 cultures in this initial state. \textbf{Bottom right}: final population at the equilibrium state, from 259 different cultural cultures randoly spread over the population, the simulation reached an equilibrium with 4 culture; one of the cultures has almost spread all over the network. \textbf{Top}: data collected during the simulation.}
	\label{fig:img3}
\end{figure}

Regarding Louvain communities, prevision \ref{itm:prev3} did not entirely hold true in the actual results of the experiments. Indeed even with configurations such that segregation is encouraged, a single culture usually spreads around different Louvain communities close to each other, without respecting the boundaries. This probably happens because a single cultures needs to flip just a few bridges in order to trigger a cascade effect on a nearby community, flipping the traits of its sites. This behaviour is evident in Figure \ref{fig:lwcomm}.

\begin{figure}
	\centering
	\includegraphics[scale=0.19]{images/lw1_1.png}
	\includegraphics[scale=0.19]{images/lw1_2.png}
	\\
	\includegraphics[scale=0.19]{images/lw2_1.png}
	\includegraphics[scale=0.19]{images/lw2_2.png}	
	\caption{\textbf{Top:} Louvain communities (left) and final cultures in the equilibrium state (right) of a network with 400 sites, average degree 6, $F=3$, $q=3$, $e=0.30$, $T=0.51$, white nodes are empty sites. The seed used to generate this simulation  is $-1929089644$. In various zones (for example in the lower left corner of the pictures) it is visible how two different Louvain communities end up sharing the same culture. \textbf{Bottom:} Louvain communities (left) and final cultures in the equilibrium state (right) of a network with 400 sites, average degree 12, $F=9$, $q=9$, $e=0.45$, $T=0.80$, white nodes are empty sites. The seed used to generate this simulation  is $1924688830$.}
	\label{fig:lwcomm}
\end{figure}

Figure \ref{fig:img2} and \ref{fig:lwcomm} also disprove the \ref{itm:prev8}th prevision: bridges do not have an intermediate value between to cultures, they either are empty sites or share the same cultural code of one of the culture they connect (this is what happens between dark purple and dark red in the lower right corner of Figure \ref{fig:img2}, between purple and brown and between purple and light blue in the top picture of Figure \ref{fig:lwcomm}).This behaviour is probably due to the fact that the algorithm behind Axelrod-Schelling model places cultures in a way such that the outskirts of a culture are compatible with the outskirts of the nearby culture without ``middlemen" on the bridges . If this wasn't the case, the middleman on the bridge would either imitate one of the nearby cultures or move to another empty sites, in order to improve its average cultural overlap.


Another prevision that instead has been confirmed by the experiments is prevision \ref{itm:prev6}, as shown also by Figure \ref{fig:img1}, Figure \ref{fig:img2} and Figure \ref{fig:img3}. Here we can notice that the more the cultural code complexity increases the more are the ticks (visible in the time axis of the ``Network status" plot) required to converge to an equilibrium state. The reasons why this happens have already been explained in Section \ref{subsec:previsions}: intuitively when code complexity is high, more time is needed for the algorithm in order to find stable configurations among all the combinations.

Regarding the \ref{itm:prev5}th prevision, it was disproved by the experiments, since with various configurations of the parameters the network converged without many problems and with a reasonable amount of NetLogo's ticks (~2000 ticks). Indeed with low empty probability and high T threshold (intolerant society), individuals must resort to imitation as the primary strategy to improve their average cultural overlap. Only 1 out of 5 experiments with this configuration ($e=20$ and $T=80$) resulted with a never-ending loop among 4 final state, but it can be considered a statistical outlier. Instead by increasing the empty probability to values around $0.30$ and setting the $T$ threshold with values close to $0.51$ we can get an outcome similar to the one described in the top right network in Figure \ref{fig:lwcomm}, where we can clearly see that all of the sites of each neighborhood have a specific culture different from the culture of sites located in other neighborhood, confirming our hypothesis about segregation expressed in prevision \ref{itm:prev4} of Section \ref{subsec:previsions}. Similar results have been obtained with other seeds and other configurations for $F$ and $q$.

\begin{figure}
	\includegraphics[scale=0.36]{images/img4.png}
	\caption{\textbf{Top:} Final state and statistics on a spatially clustered network with 400 nodes, average degree 12, $e=0.45$, $T=0.55$, $F=15$, $q=15$, seed=1632081973. \textbf{Middle:} Final state and statistics on a spatially clustered network with 400 nodes, average degree 12, $e=0.45$, $T=0.55$, $F=9$, $q=9$, seed=-1586706976. \textbf{Bottom:} Final state and statistics on a spatially clustered network with 400 nodes, average degree 12, $e=0.45$, $T=0.50$, $F=3$, $q=3$, seed=1355387318.}
	\label{fig:img4}
\end{figure}

As Figure \ref{fig:img4} suggests, there seems to be no correlation between an increased code complexity and the number of different cultures in the final state, as instead suggested by prevision \ref{itm:prev9}. This is probably due to the fact that the networks tested did not contain enough hubs and clusters. Indeed even with $q=3$ and $F=3$ there are sufficient combinations (specifically 27) to potentially fill all the clusters. For this reason further experiments are needed to deny or confirm the \ref{itm:prev9}th prevision.

\subsubsection{2D lattice: }
lattices were expected to behave similarly to the Schelling's segregation model, with segregation visible even with low thresholds values. The experiments not only confirmed this expectation even with $T=0.30$, as it is evident in Figure \ref{fig:lattice}, but also allow us to see other interesting patterns. Indeed the network tends to converge to a final state where cultures segregate and isolate themselves from the others by forming ``islands", with empty sites surrounding them and separating each culture from the others. This behaviour may be due to the fact that, contrary to what happens in the spatially clustered layout, in this layout sites have less chance to isolate themselves using hubs and clusters with few bridges connecting them to compatible neighbours cultures. Indeed there is instead an high chance of being in contact with different cultures along the outskirts, leading to the necessity of a new mechanism of isolation. Another evident behaviour is the tendency to take longer times in order to converge to an equilibrium configuration when code complexity is high, confirming even for lattices prevision \ref{itm:prev6} of Section \ref{subsec:previsions}.

\begin{figure}
	\centering
	\begin{minipage}{0.29\textwidth}
		\includegraphics[width=\linewidth]{images/lat1_1.png}
	\end{minipage}%
	\begin{minipage}{0.46\textwidth}
		\includegraphics[width=\linewidth]{images/lat1_2.png}
	\end{minipage}
	\begin{minipage}{0.29\textwidth}
		\includegraphics[width=\linewidth]{images/lat2_1.png}
	\end{minipage}%
	\begin{minipage}{0.46\textwidth}
		\includegraphics[width=\linewidth]{images/lat2_2.png}
	\end{minipage}
	\begin{minipage}{0.29\textwidth}
		\includegraphics[width=\linewidth]{images/lat3_1.png}
	\end{minipage}%
	\begin{minipage}{0.46\textwidth}
		\includegraphics[width=\linewidth]{images/lat3_2.png}
	\end{minipage}
	\caption{Final states and data of 2D lattices of 400 nodes with different configurations. Notice the presence of cultural agglomerates separated from other cultures by empty sites and how many NetLogo's ticks are required in order to reach a stable configuration.
	\textbf{Top:} $F=3$, $q=3$, $e=0.30$, $T=0.51$, seed=-761191495.
	\textbf{Middle:} $F=3$, $q=3$, $e=0.30$, $T=0.30$, seed=1988017058.
	\textbf{Bottom:} $F=15$, $q=15$, $e=0.35$, $T=0.55$, seed=1405094782.}
	\label{fig:lattice}
\end{figure}

\subsubsection{Preferential attachment:} the behaviour of the Axelrod-Schelling model in preferential attachment layouts was substantially different from the one observed in the other two layouts. With the same configuration of parameters, preferential attachment usually converges to a stable final state much faster than other layouts. Moreover, compared to other network layouts, the equilibrium state is also characterized by the presence of more cultures. This behaviour can be explained by noticing that in this kind of networks each prominent site/``influencer" (nodes with higher degree than the average ones) can have its own community and culture, as suggested by Figure \ref{fig:pref1} and Figure \ref{fig:pref2}. This also explains why in this layout a specific Louvain community usually is made up of nodes with the same cultural code.

\begin{figure}
	\centering
	\includegraphics[scale=0.39]{images/pref1.png}
	\caption{Louvain communities (bottom left), cultures in the final equilibrium state (bottom right) and data (top) of a preferential attachment network layout with 400 nodes. Configuration: $F=3$, $q=3$, $e=0.35$, $T=0.80$, seed=618376717.}
	\label{fig:pref1}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.69]{images/cod_final.png}
	\caption{Color coding of the 9 cultural traits (from 0 to 8 going from top left to bottom right) of a preferential attachment network layout with 400 nodes and data collected (bottom right). Configuration: $F=9$, $q=9$, $e=0.35$, $T=0.51$, seed=-451809446. The final state here depicted is characterized by 60 different cultures (a value greater than the number of easily distinguishable color in NetLogo, which is the reason why here the strategy for coloring involves coloring each cultural trait).}
	\label{fig:pref2}
\end{figure}

\subsubsection{Unexpected behaviours and other interesting findings: } 
By observing all the plots of the experiments it is possible to see that the changes over time of the ratio of sites with average overlap lower than 1 mimic almost perfectly the changes of the number of interactions (sites moving and imitations) over time, as shown in Figure \ref{fig:plotcmp}. This is due to the fact that most of the interactions are caused by sites trying to improve their average cultural overlap by imitating neighbours or moving and the algorithm underlying the Axelrod-Schelling model may indeed be seen as an algorithm that tries to minimize the number of nodes with average overlap lower than 1.

\begin{figure}
	\centering
	\includegraphics[scale=1.5]{images/plotcmp.png}
	\caption{Plots of one of the experiments. Differences in appearance are caused by different scaling factors.}
	\label{fig:plotcmp}
\end{figure} 